package org.abbtech.practice.lesson22.controller;

import org.abbtech.practice.lesson22.Test.TestClass;
import org.abbtech.practice.lesson22.enums.EnumSecured;
import org.abbtech.practice.lesson3.repository.TestRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    private final TestClass testClass;
    public TestController(TestClass testClass){
        this.testClass=testClass;

    }
    @GetMapping("/1")
    public void test1(){
        testClass.unsecuredMethod();
    }

    @GetMapping("/2")
    public void test2(){
        testClass.securedMethod(EnumSecured.OTHER);
    }

}
