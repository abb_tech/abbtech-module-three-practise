package org.abbtech.practice.lesson22.annotation;

import org.abbtech.practice.lesson22.enums.EnumLogging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.CONSTRUCTOR})
public @interface Loggable {
    EnumLogging value() default EnumLogging.HIGH;
}
