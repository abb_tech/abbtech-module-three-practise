package org.abbtech.practice.lesson22.annotation;

import org.abbtech.practice.lesson22.enums.EnumSecured;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.CONSTRUCTOR})
public @interface Secured {
    EnumSecured[] value() default EnumSecured.ADMIN;
}

