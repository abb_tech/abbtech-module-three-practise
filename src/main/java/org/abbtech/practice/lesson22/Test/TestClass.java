package org.abbtech.practice.lesson22.Test;

import org.abbtech.practice.lesson22.annotation.Loggable;
import org.abbtech.practice.lesson22.annotation.Secured;
import org.abbtech.practice.lesson22.enums.EnumSecured;
import org.springframework.stereotype.Component;

@Component
public class TestClass {
    @Loggable
    public void unsecuredMethod() {
        System.out.println("Inside loggable");
    }

    @Secured(value = {EnumSecured.USER,EnumSecured.OTHER})
    public void securedMethod(EnumSecured  role) {
        System.out.println("Inside securedMethod");
    }

}
