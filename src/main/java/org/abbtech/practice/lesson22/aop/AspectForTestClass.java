package org.abbtech.practice.lesson22.aop;

import org.abbtech.practice.lesson22.annotation.Loggable;
import org.abbtech.practice.lesson22.annotation.Secured;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectForTestClass {
    @Pointcut("execution(* org.abbtech.practice.lesson22.Test.TestClass.*(..))")
    public void example(){

    };

    @Before("@annotation(loggable)")
    public void unsecured(Loggable loggable){
        System.out.println(loggable.value());

    }

    @Before("@annotation(secured)")
    public void secured(JoinPoint joinPoint, Secured secured){
        for (Object i : secured.value()){
            if (joinPoint.getArgs()[0].equals(i)){
                System.out.println("Authorized");
                return;
            }
        }
        System.out.println("Not Authorized");
    }

}
