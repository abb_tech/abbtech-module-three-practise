package org.abbtech.practice.lesson2.service;

import org.abbtech.practice.lesson2.model.Task;
import org.abbtech.practice.lesson2.repository.TaskRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    private final TaskRepository taskRepository;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getAllTasks() {
        return taskRepository.getAllTasks().get();
    }

    public String createTask(List<Task> tasks) {
        taskRepository.createTask(tasks);
        return "success";
    }
    public void saveTask(Task tasks) {
        taskRepository.saveTask(tasks);
    }
    public Task getTaskById(Long id) {
        Task task = taskRepository.getTaskById(id).get();
        return task;
    }

    public void updateTask(Long id, Task task) {
        taskRepository.updateTask(id, task);
    }

    public void deleteTask(Long id) {
        taskRepository.deleteTask(id);
    }

}
