package org.abbtech.practice.lesson2.controller;

import org.abbtech.practice.lesson2.model.Task;
import org.abbtech.practice.lesson2.service.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService)
    {
        this.taskService = taskService;
    }
    @PostMapping("/add")
    public String postTasks(@RequestBody List<Task> task){
        return taskService.createTask(task);
    };
    @PostMapping("/save")
    public ResponseEntity<String> saveTask(@RequestBody Task task){
        taskService.saveTask(task);
        return new ResponseEntity<>("Task Successfully added",HttpStatus.CREATED);
    }
    @GetMapping("/get/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<>(taskService.getTaskById(id), HttpStatus.OK);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<Task>> getAllTasks(){
        return new ResponseEntity<>(taskService.getAllTasks(), HttpStatus.OK);
    }
    @PatchMapping ("/update/{id}")
    public ResponseEntity<String> updateTask(@PathVariable(value = "id") Long id,@RequestBody Task task){
        taskService.updateTask(id,task);
        return new ResponseEntity<>("Successfully updated",HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteTask(@PathVariable Long id){
        taskService.deleteTask(id);
        return new ResponseEntity<>("Task with id of "+id+" is successfully deleted",HttpStatus.OK);
    }
}
