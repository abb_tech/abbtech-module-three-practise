package org.abbtech.practice.lesson2.repository;

import org.abbtech.practice.lesson2.model.Task;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class TaskRepository {
    private JdbcTemplate jdbcTemplate;

    public TaskRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void createTask(List<Task> tasks) {
        jdbcTemplate.batchUpdate(
                "INSERT INTO TASKS(task_name) VALUES (?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, tasks.get(i).getName());
                    }

                    @Override
                    public int getBatchSize() {
                        return tasks.size();
                    }
                }
        );
    }

    public Optional<List<Task>> getAllTasks() {
        List<Task> tasks =
                jdbcTemplate.query("""
                            SELECT * FROM TASKS
                        """, (rs, rowNum) -> new Task(rs.getLong("task_id"), rs.getString("task_name")));
        return Optional.of(tasks);
    }
    public void saveTask(Task task){
        jdbcTemplate.update("""
        INSERT INTO TASKS(task_name) VALUES (?)
        """,task.getName());
    }

    public Optional<Task> getTaskById(Long id) {
        try {
            Task taskDto = jdbcTemplate.queryForObject(
                    "SELECT * FROM TASKS WHERE id = ?",
                    (rs, rowNum) -> new Task(rs.getLong("task_id"), rs.getString("task_name")),
                    id
            );
            return Optional.ofNullable(taskDto);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void updateTask(Long id, Task task) {
        if (getTaskById(id).isPresent()) {
            jdbcTemplate.update("""
                                UPDATE Tasks SET name = ? WHERE id = ?
                    """, task.getName(), id);
        } else {
            throw new IllegalArgumentException("Invalid id" + id);
        }
    }

    public void deleteTask(Long id){
        if (getTaskById(id).isPresent()){
            jdbcTemplate.update("""
        DELETE from Tasks where id=?
        """,id);
        }else throw new IllegalArgumentException("No task with "+id);

    }


}
