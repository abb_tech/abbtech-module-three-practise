package org.abbtech.practice.lesson2.model;

public class Task {
    private Long id;
    private String name;

    public Task(long id, String taskName) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
