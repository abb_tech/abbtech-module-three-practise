package org.abbtech.practice.lesson1;

import org.springframework.stereotype.Service;

@Service
public class ApplicationService {
    private CalculationService calculationService;

    public ApplicationService(CalculationService calculationService) {
        this.calculationService =calculationService;
    }

    public int addition(int a, int b) {
        if (a < 0 || b < 0) {
            throw new ArithmeticException("Both number must be positive");
        }
        return calculationService.addition(a,b);
    }

    public double multiply(int a, int b) {
        if (a > 4 && b > 6) {
            throw new ArithmeticException("a is grt 4, b is grt 6");
        }
        return calculationService.multiply(a, b);
    }

    public double subtraction(int a, int b) {
        var result = calculationService.subtraction(a, b);
        if (result == 2 || result == 4 || result == 6 || result == 8) {
            throw new ArithmeticException("2, 4, 6, 8 not allowed");
        }
        return result;
    }

    public double division(int a, int b) {
        if (a==0 || b == 0) {
            throw new ArithmeticException("Parametrs cannot be zero.");
        }
        if (a % 2 != 0 || b % 2 != 0) {
            throw new ArithmeticException("Both number must be divisible by 2.");
        }
        return calculationService.division(a,b);
    }

}
