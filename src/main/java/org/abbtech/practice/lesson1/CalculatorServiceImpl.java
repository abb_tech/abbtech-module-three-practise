package org.abbtech.practice.lesson1;

import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculationService{
    @Override
    public int addition(int a, int b) {
        return a+b;
    }

    @Override
    public int subtraction(int a, int b) {
        return a-b;
    }

    @Override
    public int multiply(int a, int b) {
        return a*b;
    }

    @Override
    public int division(int a, int b) {
        return a/b;
    }
}
