package org.abbtech.practice.lesson1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sample")
public class CalculateController {
    @Autowired
    ApplicationService applicationService;
    @PostMapping("/post")
    public int calculatePost(@RequestBody CalculationDTO result){
        return applicationService.addition(result.a(), result.b());
    }
}
