package org.abbtech.practice.lesson1;

import org.springframework.stereotype.Service;

@Service
public interface CalculationService {
    int addition(int a, int b);
    int subtraction(int a, int b);
    int multiply(int a, int b);
    int division(int a, int b);
}
