package org.abbtech.practice.lesson3.services;


import org.abbtech.practice.lesson3.model.Student;
import org.abbtech.practice.lesson3.repository.TestRepository;
import org.springframework.stereotype.Service;

@Service
public class TestService {
    private final TestRepository testRepository;

    public TestService(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    public void testGet(Student student){
        testRepository.save(student);
    }
}
