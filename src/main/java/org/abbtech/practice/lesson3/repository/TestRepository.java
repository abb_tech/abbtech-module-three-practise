package org.abbtech.practice.lesson3.repository;

import org.abbtech.practice.lesson3.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Student,Long> {
}
