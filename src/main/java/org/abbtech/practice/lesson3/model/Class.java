package org.abbtech.practice.lesson3.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "class")
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ClassID;
    @Column
    private String className;


    @ManyToMany
    @JoinTable(name = "StudentClass", joinColumns = @JoinColumn(name = "ClassID",unique = true),
            inverseJoinColumns = @JoinColumn(name = "StudentID",unique = true))
    private List<Student> studentList;
}
