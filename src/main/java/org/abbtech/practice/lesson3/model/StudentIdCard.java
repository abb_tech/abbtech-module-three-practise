package org.abbtech.practice.lesson3.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "studentidcard")
public class StudentIdCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long CardID;
    @Column
    private String CardNumber;
    @Column
    private Date ExpiryDate;
    @OneToOne(mappedBy = "studentIdCard")
    private Student student;

}
