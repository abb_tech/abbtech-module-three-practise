package org.abbtech.practice.lesson3.model;

import jakarta.persistence.*;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long BookID;
    @Column
    private String title;
    @Column
    private String author;
    @OneToOne(mappedBy = "book")
    private Professor professor;

}
