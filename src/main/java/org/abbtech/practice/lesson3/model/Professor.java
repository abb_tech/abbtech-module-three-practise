package org.abbtech.practice.lesson3.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "professor")
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ProfessorID;
    @Column
    private String firstname;
    @Column
    private String lastname;

    @ManyToOne
    @JoinColumn(name = "DepartmentID", referencedColumnName = "DepartmentID")
    private Department department;

    @OneToOne
    @JoinColumn(name = "BookID", referencedColumnName = "BookID")
    private Book book;
}
