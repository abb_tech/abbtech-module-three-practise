package org.abbtech.practice.lesson3.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long DepartmentID;
    @Column
    private String departmentName;
    @OneToMany(mappedBy = "department")
    private List<Professor> professorList;
}
