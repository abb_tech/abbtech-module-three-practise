package org.abbtech.practice.lesson3.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long StudentID;

    public Long getStudentID() {
        return StudentID;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public StudentIdCard getStudentIdCard() {
        return studentIdCard;
    }

    public List<Class> getClasses() {
        return classes;
    }

    @Column
    private String firstname;
    @Column
    private String lastname;

    @OneToOne
    @JoinColumn(name = "CardID", referencedColumnName = "CardID")
    private StudentIdCard studentIdCard;
    @ManyToMany(mappedBy = "studentList")
    private List<Class> classes;
}
