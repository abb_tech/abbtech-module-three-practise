package org.abbtech.practice.lesson3.controller;

import org.abbtech.practice.lesson3.model.Student;
import org.abbtech.practice.lesson3.services.TestService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/library")
public class LibraryController {
    private final TestService testService;

    public LibraryController(TestService testService) {
        this.testService = testService;
    }

    @PostMapping ("/salam")
    public void getbook(@RequestBody Student student){
        testService.testGet(student);
    }
}
